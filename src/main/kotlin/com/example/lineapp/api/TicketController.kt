package com.example.lineapp.api

import com.example.lineapp.service.TicketService
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDate
import java.util.*

@RestController
class TicketController(
    private val ticketService: TicketService
) {
    @GetMapping("/tickets")
    fun getTicket(@RequestParam("date")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) date: LocalDate
    ): String {
        return ticketService.getTicket(date)
    }
}