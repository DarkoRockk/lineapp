package com.example.lineapp.service

import com.example.lineapp.dao.TicketDao
import org.springframework.stereotype.Service
import java.time.LocalDate

@Service
class TicketService(
    private val ticketRepository: TicketDao
) {
    fun getTicket(date: LocalDate): String {
        return "$date-${String.format("%04d", ticketRepository.getNumber(date))}"
    }
}