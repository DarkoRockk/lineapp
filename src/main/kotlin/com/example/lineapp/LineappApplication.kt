package com.example.lineapp

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class LineappApplication

fun main(args: Array<String>) {
	/**
	 * 0. java.util.Date java.util.Timestamp -- never use
	 *
	 * 1. LocalDate -- YYYY-MM-DD
	 * 2. LocalTime -- HH:MM
	 * 3. LocalDateTime -- date + time
	 *
	 * 4. Offset / Zoned -- timezone is matter
	 */
	runApplication<LineappApplication>(*args)
}
