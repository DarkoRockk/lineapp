package com.example.lineapp.dao

import java.time.LocalDate

interface TicketDao {
    fun getNumber(date: LocalDate): Int?
}