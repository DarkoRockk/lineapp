package com.example.lineapp.dao

import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Repository
import java.time.LocalDate

@Repository
class TicketRepository(
    private val jdbcTemplate: JdbcTemplate
) : TicketDao {

    override fun getNumber(date: LocalDate): Int? {
        val sql1 = "CREATE SEQUENCE IF NOT EXISTS \"$date\" START 1 INCREMENT 1;"
        jdbcTemplate.execute(sql1)
        val sql2 = "SELECT nextval('$date');"
        return jdbcTemplate.queryForObject(sql2, Int::class.java)
    }
}